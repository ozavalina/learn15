package ru.oz.learn15;

import lombok.Data;

@Data
public class Datum {
    private String breed;
    private String country;
    private String origin;
    private String coat;
    private String pattern;
}