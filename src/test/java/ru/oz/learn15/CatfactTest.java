package ru.oz.learn15;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.*;

public class CatfactTest {
    RequestSpecification requestSpecification = new RequestSpecBuilder()
            .log(LogDetail.ALL)
            .build();

    ResponseSpecification responseSpecification = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .expectContentType(JSON)
            .expectStatusCode(200)
            .expectHeader("Content-Type", "application/json")
            .build();

    // BDD given() - предусловие when() - совершаемое действие then() - ожидания от этого действия, те проверки

    @Test(description = "Проверка получения верных значений полей объекта по первой породе")
    public void testGetRightValues() {
        Datum dataTest = new Datum();
        dataTest.setBreed("Abyssinian");
        dataTest.setCountry("Ethiopia");
        dataTest.setOrigin("Natural/Standard");
        dataTest.setCoat("Short");
        dataTest.setPattern("Ticked");
        Datum datum = RestAssured
                .given()
                .spec(requestSpecification)
                .baseUri("https://catfact.ninja/breeds")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .extract().body().jsonPath().getObject("data[0]", Datum.class);
        Assert.assertEquals(datum, dataTest);
    }

    @Test(description = "Проверка, что при указании параметра limit = 1 вторая порода не определяется")
    public void limitParameterTest() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .queryParam("limit", 1)
                .baseUri("https://catfact.ninja/breeds")
                .when()
                .get() // https://catfact.ninja/breeds?limit=1
                .then()
                .spec(responseSpecification)
                .body("data[1].breed", equalTo(null));
    }

    @Test(description = "Проверка, что при максимальной длине строки = 19 тело ответа - '{}'")
    public void emptyResponseBodyIsReturnedTest() {
        String responseString = RestAssured
                .given()
                .spec(requestSpecification)
                .queryParam("max_length", 19)
                .baseUri("https://catfact.ninja/fact")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .extract().body().asString();
        Assert.assertEquals(responseString, "{}");
    }

    @Test(description = "Проверка, что в ответе содержатся поля fact и length и нет поля fact123")
    public void fieldPresenceTest() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .baseUri("https://catfact.ninja/fact")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .body("$", hasKey("fact"))
                .body("$", hasKey("length"))
                .body("$", not(hasKey("fact123")));
    }

    @Test(description = "Проверка части полей")
    public void testSomeFields() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .baseUri("https://catfact.ninja/facts")
                .when()
                .get()
                .then()
                .spec(responseSpecification)
                .assertThat()
                .body("total", equalTo(332))
                .body("per_page", equalTo(10))
                .body("prev_page_url", equalTo(null))
                .body("to", equalTo(10))
                .body("from", equalTo(1))
                .body("last_page", equalTo(34));
    }
}
